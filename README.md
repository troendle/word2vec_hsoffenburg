# Word2Vec_HSOffenburg
Zwei kleine Jupyter-Notebooks für die Probelevorlesung "Einführung in den Word2Vec Algorithmuss" an der Hochschule Offenburg.

Die Daten - der Satz-Corpus und das vortrainierte Modell - können unter folgendem Link heruntergeladen werden: 


https://www.dropbox.com/sh/avv4yq95foybxd9/AABSeqxpxOqKP54mCT1H6sffa?dl=0
